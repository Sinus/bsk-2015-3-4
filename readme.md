A program that securely:

- reads unknown number of lines (until finished with a dot)

- prints the lines in which at least 1 word is repeated even number of times, print that word and number of repeats

- use PAM to require authentication before using the binary
