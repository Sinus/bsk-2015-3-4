#ifndef _TYPES_H_
#define _TYPES_H_
struct wordBuilder
{
    char c;
    struct wordBuilder* next;
};

struct line
{
    char* word;
    struct line* next;
    int arity;
};
#endif
