#include <stdio.h>
#include <stdlib.h>
#include <security/pam_appl.h>
#include <security/pam_misc.h>
#include "err.h"
#include "types.h"

static struct pam_conv login_conv = {
    misc_conv,
    NULL
};

void authenticate()
{
    pam_handle_t* pamh = NULL;
    int retval;
    char* username = NULL;
    char* prompt = "Kto to? ";


    retval = pam_start("secure_even", username, &login_conv, &pamh);
    if (pamh == NULL || retval != PAM_SUCCESS)
        fatal("pam_start error", retval);

    if (pam_acct_mgmt(pamh, PAM_SILENT) != PAM_SUCCESS)
    {
        printf("Access denied\n");
        fatal("pam_acct_mgmt error");
    }

    retval = pam_set_item(pamh, PAM_USER_PROMPT, (void *) prompt);
    if (retval != PAM_SUCCESS)
        fatal("pam_set_item error", retval);

    if (pam_authenticate(pamh, PAM_SILENT) != PAM_SUCCESS)
    {
        printf("Access denied\n");
        fatal("pam_authenticate error");
    }

    pam_end(pamh, PAM_SUCCESS);
}

char* toString(struct wordBuilder* word)
{
    if (word == NULL)
        return NULL;
    int length = 0;
    struct wordBuilder* i = word;
    while (i != NULL)
    {
        length += 1;
        i = i->next;

        //length can be very big, (>> int)
        //so we will fail if the word is too long
        //to dodge buffer overflow and potent attacks
        if (length >= 10000)
            fatal("Word is too long");
        //we won't worry about releasing the memory for now
        //formally a memory leak
    }

    char* result = malloc(sizeof(char) * (length + 1));
    memset(result, 0, sizeof(char) * (length + 1));
    if (result == NULL)
        fatal("Malloc failed");

    i = word;
    int j = 0;
    while (i != NULL)
    {
        result[j] = i->c;
        i = i->next;
        j += 1;
    }
    return result;
}

void deleteWord(struct wordBuilder* word)
{
    struct wordBuilder* i = word;
    while (i != NULL)
    {
        struct wordBuilder* j = i;
        i = i->next;
        free(j);
    }
    word = NULL;
}

void deleteLine(struct line* l)
{
    struct line* i = l;
    while (i != NULL)
    {
        struct line* j = i;
        i = i->next;
        free(j->word);
        free(j);
    }
    l = NULL;
}

void addWord(char* word, struct line** lineStart, struct line** lineHead)
{
    if (*lineStart == NULL)
    {
        *lineStart = malloc(sizeof(**lineStart));
        if (*lineStart == NULL)
            fatal("Malloc failed");

    }
    if (*lineHead == NULL)
        *lineHead = *lineStart;
    else
    {
        struct line* next = malloc(sizeof(*next));
        if (next == NULL)
            fatal("Malloc failed");
        (*lineHead)->next = next;
        *lineHead = next;
    }
    (*lineHead)->word = word;
    (*lineHead)->next = NULL;
    (*lineHead)->arity = 1;
}

void addLetter(char c, struct wordBuilder** wordStart, struct wordBuilder** wordHead)
{
    if (c == '\n')
        return;
    struct wordBuilder* next;
    if (*wordStart == NULL)
    {
        *wordStart = malloc(sizeof(**wordStart));
        if (*wordStart == NULL)
            fatal("Malloc failed");
    }
    if (*wordHead == NULL)
        *wordHead = *wordStart;
    else
    {
        next = malloc(sizeof(*next));
        if (next == NULL)
            fatal("Malloc failed");
        (*wordHead)->next = next;
        *wordHead = next;
    }
    (*wordHead)->c = c;
    (*wordHead)->next = NULL;
}

struct line* checkEven(struct line** lineStart)
{
    struct line* iter = *lineStart;
    while (iter != NULL)
    {
        if (iter->arity % 2 == 0)
            return iter;
        iter = iter->next;
    }
    return NULL;
}

short handleLine()
{
    char in;
    struct line* lineStart = NULL;
    struct line* lineHead = NULL;
    struct wordBuilder* wordStart = NULL;
    struct wordBuilder* prev = NULL;    
    struct wordBuilder* fullLine = NULL;
    struct wordBuilder* fullLineHead = NULL;
    short dot = 0; //dot in line means that we will be ending our job

    while (scanf("%c", &in) == 1)
    {
        //printf("in: %c, strcmp: %d\n", in, strcmp(&in, "\n"));
        addLetter(in, &fullLine, &fullLineHead);
        if (in == '.')
            dot = 1;
        if (in == ' ' || in == '\n')
        {
            int found = 0;
            if (wordStart != NULL)
            {
                //real word ended, add it to line
                char* currWord = toString(wordStart);
                //check if currWord exists in line
                struct line* iter = lineStart;
                while (iter != NULL)
                {
                    if (strcmp(iter->word, currWord) == 0)
                    {
                        //we found the word
                        iter->arity += 1;
                        found = 1;
                    }
                    iter = iter->next;
                }

                if (found == 0) //new word
                    addWord(currWord, &lineStart, &lineHead);
                else
                    free(currWord);

                deleteWord(wordStart);
                wordStart = NULL;
                prev = NULL;
            }
        }
        else
            addLetter(in, &wordStart, &prev); 
        if (in == '\n')
            break;
    }

    struct line* even = checkEven(&lineStart);
    char* full = toString(fullLine);
    if (even != NULL) //there is a word with even parity
        printf("(%s: %d) %s\n", even->word, even->arity, full);

    //free the memory
    free(full);
    deleteLine(lineStart);
    deleteWord(fullLine);
    return dot;
}

int main()
{
    authenticate();

    while (handleLine() == 0)
        continue;
    return 0;
}
